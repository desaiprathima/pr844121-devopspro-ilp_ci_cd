package com.desai.ilp1.login;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runner.Result;
import org.junit.runner.JUnitCore;
import org.junit.runner.notification.*;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({LoginValidateTestCase2.class,LoginValidateTest.class})

public class LoginValidateTestSuite {

@BeforeClass
   public static void printMe() {
	System.out.println("LoginValidTestSuite is the test suite grouping loginvalidatetest and test2");
}

}
