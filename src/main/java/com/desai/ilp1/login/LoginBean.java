package com.desai.ilp1.login;
	public class LoginBean {  
		private static String email;
		private static String pass;  
		  
		public static String getEmail() {  
		    return email;  
		}  
		  
		public void setEmail(String email) {  
		    LoginBean.email = email;  
		}  
		  
		public static String getPass() {  
		    return pass;  
		}  
		  
		public void setPass(String pass) {  
		    LoginBean.pass = pass;  
		}  
		  
		  
		}  
